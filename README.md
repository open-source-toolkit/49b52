# MybatisLog - 提升MyBatis调试效率的SQL日志插件

## 欢迎使用 MybatisLog！

### 简介
MybatisLog 是专为简化和优化MyBatis框架下SQL日志查看而设计的免费插件。在日常开发中，调试和优化SQL性能是必不可少的一环。本插件通过增强MyBatis的日志输出，使其更加直观、易读，极大提升了开发者在进行数据库操作时的效率。

### 特性
- **自动格式化**：将原始的SQL日志转换成易读格式，包括参数绑定值的明确显示。
- **高亮显示**：在控制台中以颜色区分SQL语句与参数，提高辨识度。
- **减少噪音**：智能过滤不必要的日志信息，专注于关键的SQL执行过程。
- **兼容性强**：适配多种日志框架（如Logback, Log4j2等），无需复杂的配置即可集成到现有项目中。
- **易于部署**：轻量级设计，添加依赖后几乎零配置即可使用，适合快速集成进任何基于MyBatis的应用。

### 快速开始

#### 添加依赖

 Maven用户可以在`pom.xml`加入如下依赖：
 
 ```xml
 <dependency>
     <groupId>com.example</groupId>
     <artifactId>mybatis-log</artifactId>
     <version>1.0.0</version> <!-- 请替换为实际版本号 -->
 </dependency>
 ```

 Gradle用户可以在你的`build.gradle`文件中添加：

 ```groovy
 implementation 'com.example:mybatis-log:1.0.0' // 请替换为实际版本号
 ```

#### 配置启用

默认情况下，MybatisLog在添加依赖后即自动启用，但建议根据具体需要调整日志级别以确保只捕获到必要的SQL日志信息。

对于常见的日志框架，例如Logback，可以在`logback.xml`中设置：

```xml
<logger name="com.example.mybatislog" level="DEBUG"/>
```

#### 使用示例

一旦集成完毕，当你运行应用并执行MyBatis的查询时，将在控制台看到美化后的SQL日志输出。

### 注意事项

- 确保你的应用程序具有正确的日志配置，以便能够捕捉到DEBUG级别的日志。
- 在生产环境中，适当调整日志级别以避免性能影响和敏感信息泄露。
- 根据插件更新及时升级，以利用最新功能和修复可能存在的问题。

### 社区与贡献

欢迎加入我们的社区，无论是问题反馈、建议还是代码贡献，我们都热烈欢迎。请通过GitHub的Issue页面提交你的反馈，或者考虑fork仓库并发起Pull Request来贡献力量。

### 结论

MybatisLog旨在让MyBatis的使用体验更上一层楼，帮助开发者更快地定位问题和优化查询。希望这个工具能成为你开发过程中的得力助手！

---

请注意，上述内容中的版本号、groupId和artifactId应根据实际情况调整。在使用前，也请访问项目的官方GitHub页面获取最新的安装和使用指南。